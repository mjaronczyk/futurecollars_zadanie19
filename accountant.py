


class Manager:

    def __init__(self):
        self.balance = 0
        self.assortment = {}   
        self.history = []
        self.load_data() 
        

    def load_data(self):
            with open("C:/Users/mjaro/Desktop/zadania/19/zadanie/balance.txt") as balance:
                self.balance = int(balance.read())
                
            with open("C:/Users/mjaro/Desktop/zadania/19/zadanie/history.txt") as history:
                for element in history.readlines():
                    self.history.append(element)
            try:
                self.history = [line.strip() for line in self.history]
            except:
                print("Something Wrong at line 35")

            with open("C:/Users/mjaro/Desktop/zadania/19/zadanie/assortment.txt") as assortment:
                for element in assortment.readlines():
                    if len(element.split()) == 2:
                        self.assortment[element.split()[0]] = int(element.split()[1])
                    else:
                        key = ""
                        for index in range(len(element.split())-1):
                            key = key + str(element.split()[index]) + " "
                        self.assortment[key[:-1]] = int(element.split()[-1])


    def decorator(func):
        def inner(self):
            with open("C:/Users/mjaro/Desktop/zadania/19/zadanie/balance.txt", "w") as file:
                file.write(str(self.balance))
                print(f'balance: {self.balance}')
            if func(self):
                try:
                    with open("C:/Users/mjaro/Desktop/zadania/19/zadanie/history.txt", "a") as file:
                        file.write(f'{self.type}, kwota: {self.price}, opis: {self.comment}\n')
                        print("saldo dziala chyba")
                except:
                    with open("C:/Users/mjaro/Desktop/zadania/19/zadanie/history.txt", "a") as file:
                        file.write(f'{self.type}, id: {self.id}, cena za sztuke: {self.price}, ilosc sztuk: {self.qty}\n')
                    with open("C:/Users/mjaro/Desktop/zadania/19/zadanie/assortment.txt", "w") as file:
                        for line in self.assortment:
                            a = f'{line} {str(self.assortment[line])}\n'
                            file.write(a)
        return inner


    @decorator
    def saldo(self):
        self.price = int(input("Amount: "))
        comment = input("Comment: ")
        self.balance += self.price
        self.type = "Saldo"
        self.comment = comment
        return True


    @decorator
    def sprzedaz(self):
        self.id = input("Podaj id produktu: ")
        self.price = int(input("Podaj cene za sztuke: "))
        self.qty = int(input("Ilosc szcztuk: "))
        self.type = "sprzedaz"
        try:
            if (self.price > 0 and self.qty > 0) and (self.assortment.get(self.id) >= self.qty):
                self.balance += self.price * self.qty
                self.assortment[self.id] -= self.qty
                self.history.append((f'Sold, id: {self.id}, price: {self.price}, qty: {self.qty}'))
                print(f'Sold, id: {self.id}, price: {self.price}, qty: {self.qty}')
            else:
                print("Error occured. Make sure qty and price are correct")
        except:
            print("Make sure the product is in stock ")
        return True


    @decorator
    def kupno(self):
        self.id = input("Podaj id produktu: ")
        self.price = int(input("Podaj cene za sztuke: "))
        self.qty = int(input("Ilosc szcztuk: "))
        self.type = "kupno"
        if (self.price * self.qty < self.balance) and (self.qty > 0 and self.price > 0):
            self.balance += -self.price * self.qty
            print("weszlo w ifa")
            try:
                self.assortment[self.id] += self.qty
            except:
                self.assortment[self.id] = self.qty
            return True
        else:
            print("Error occured. Try again!")
            return False



mgr = Manager()
mgr.saldo()